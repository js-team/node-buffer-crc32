# Installation
> `npm install --save @types/buffer-crc32`

# Summary
This package contains type definitions for buffer-crc32 (https://github.com/brianloveswords/buffer-crc32).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/buffer-crc32

Additional Details
 * Last updated: Mon, 31 Dec 2018 16:47:15 GMT
 * Dependencies: @types/node
 * Global values: none

# Credits
These definitions were written by BendingBender <https://github.com/BendingBender>.
